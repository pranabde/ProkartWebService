﻿using ProkartWebService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace ProKart.Models
{
    public class Brands
    {
        public string folderPath = "";
        public string fileName = "";
        /// <summary>
        /// Creating Database instance
        /// </summary>
        prokartdbEntities db = new prokartdbEntities();
        /// <summary>
        /// Updating brands to Database
        /// </summary>
        /// <auther>Pranab Kumar De</auther>
        /// <created>01/05/2016</created>
        /// <param name="tier1"></param>
        /// <param name="tier2"></param>
        /// <param name="tier3"></param>
        public void updateBrandsToDb(string[] tier1, string[] tier2, string[] tier3)
        {
            try
            {
                foreach (brand b in db.brands)
                {
                    if (tier1.Contains(b.brand_name))
                    {
                        var list = new List<string>(tier1);
                        list.Remove(b.brand_name);
                        tier1 = list.ToArray();
                    }
                    if (tier2.Contains(b.brand_name))
                    {
                        var list = new List<string>(tier2);
                        list.Remove(b.brand_name);
                        tier2 = list.ToArray();
                    }
                    if (tier3.Contains(b.brand_name))
                    {
                        var list = new List<string>(tier3);
                        list.Remove(b.brand_name);
                        tier3 = list.ToArray();
                    }
                }
                for (int i = 0; i < tier1.Length; i++)
                {
                    brand brand = new brand();
                    brand.brand_name = tier1[i];
                    brand.brand_tier = "tier_1";
                    db.brands.Add(brand);
                    db.SaveChanges();
                }
                for (int i = 0; i < tier2.Length; i++)
                {
                    brand brand = new brand();
                    brand.brand_name = tier2[i];
                    brand.brand_tier = "tier_2";
                    db.brands.Add(brand);
                    db.SaveChanges();
                }
                for (int i = 0; i < tier3.Length; i++)
                {
                    brand brand = new brand();
                    brand.brand_name = tier3[i];
                    brand.brand_tier = "tier_3";
                    db.brands.Add(brand);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Fetching values brand from Database 
        /// <auther>Pranab Kumar De</auther>
        /// <created>01/05/2016</created>
        /// </summary>
        /// <returns></returns>
        public SortedDictionary<string, List<string>> getBrandsFromDb()
        {
            SortedDictionary<string, List<string>> sd = new SortedDictionary<string, List<string>>();
            List<string> tier1 = new List<string>();
            List<string> tier2 = new List<string>();
            List<string> tier3 = new List<string>();
            try
            {
                foreach (brand b in db.brands)
                {
                    if (b.brand_tier == "tier_1")
                    {
                        tier1.Add(b.brand_name);
                    }
                    if (b.brand_tier == "tier_2")
                    {
                        tier2.Add(b.brand_name);
                    }
                    if (b.brand_tier == "tier_3")
                    {
                        tier3.Add(b.brand_name);
                    }
                }
                sd.Add("Tier 1", tier1);
                sd.Add("Tier 2", tier2);
                sd.Add("Tier 3", tier3);
                return sd;
            }
            catch (Exception e)
            {
                sd.Clear();
                throw e;
            }
        }
    }
}
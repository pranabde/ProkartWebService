﻿using ProKart.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace ProkartWebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IProkartWebServices" in both code and config file together.
    [ServiceContract]
    public interface IProkartWebServices
    {
        /// <summary>
        /// Sell Section
        /// </summary>
        /// 
        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json, 
        UriTemplate = "/SetDeviceDetails")]
        [FaultContract(typeof(ProkartCustomException))]
        string SetDeviceDetails(Device data);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/SetDeviceDetailsAfter")]
        [FaultContract(typeof(ProkartCustomException))]
        string SetDeviceDetailsAfter(Device data);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/SetDeviceSpecification")]
        [FaultContract(typeof(ProkartCustomException))]
        string SetDeviceSpecification(Device data);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetDevicePrice")]
        [FaultContract(typeof(ProkartCustomException))]
        string GetDevicePrice(Device data);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/CheckOut")]
        [FaultContract(typeof(ProkartCustomException))]
        string CheckOut(Device data);

        [OperationContract]
        [WebInvoke(Method = "POST", 
            ResponseFormat = WebMessageFormat.Json, 
            UriTemplate = "/GetDevicePic")]
        [FaultContract(typeof(ProkartCustomException))]
        string GetDevicePic();

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/SetDeviceDetailsForOtherDevices")]
        [FaultContract(typeof(ProkartCustomException))]
        string SetDeviceDetailsForOtherDevices(Device data);

         /// <summary>
         /// Buy Section
         /// </summary>
         
        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetAllAvailableItems")]
        [FaultContract(typeof(ProkartCustomException))]
        string GetAllAvailableItems(int[] index);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetSortedItems")]
        [FaultContract(typeof(ProkartCustomException))]
        string GetSortedItems(string[] brands);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetAllAvailableBrands")]
        [FaultContract(typeof(ProkartCustomException))]
        string GetAllAvailableBrands();

        /// <summary>
        /// Admin Section
        /// </summary>
        /// 
        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, 
        RequestFormat = WebMessageFormat.Json,
        UriTemplate = "/SetBrands")]
        [FaultContract(typeof(ProkartCustomException))]
        void SetBrands(string[] Tier1, string[] Tier2, string[] Tier3);

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetBrands")]
        [FaultContract(typeof(ProkartCustomException))]
        string GetBrands();

        [OperationContract]
        [WebInvoke(Method = "POST",
        RequestFormat = WebMessageFormat.Json,
        ResponseFormat = WebMessageFormat.Json,
        UriTemplate = "/GetGadgetData")]
        [FaultContract(typeof(ProkartCustomException))]
        string GetGadgetData();

        [OperationContract]
        [WebInvoke(Method = "POST", BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "/UpdateGadgetStatus")]
        [FaultContract(typeof(ProkartCustomException))]
        void UpdateGadgetStatus(long[] orderIds, string status);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "/UpdateGadgetDetails")]
        [FaultContract(typeof(ProkartCustomException))]
        void UpdateGadgetDetails(List<Device> data);        
    }
}

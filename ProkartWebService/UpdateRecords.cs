﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using ProkartWebService;
using System.Data.Entity.Validation;
using Amazon.S3;
using Amazon.S3.Model;

namespace ProKart.Models
{
    public class UpdateRecords
    {
        //creating database instance
        prokartdbEntities db = new prokartdbEntities();
        public enum Availability { processing, available, sold, cancelled } //Enum having four posible values for availability

        /// <summary>
        /// <author>Pranab Kumar De</author>
        /// <date>8/7/2016</date>
        /// generating random number to be used as order id and gadget id(pk) in database
        /// </summary>
        /// <returns>order id</returns>
        public int[] IdGenerator()
        {
            Random rnd = new Random();
            int orderId = 0;
            int gadgetId = 0;
            int[] keys = new int[2];
            bool match = false;
            do
            {
                keys[0] = 0;
                orderId = rnd.Next(10000000, 99999999);
                keys[0] = orderId;
                match = db.gadgets.Any(m => m.order_id == orderId);
            }
            while (match == true);
            do
            {
                keys[1] = 0;
                gadgetId = rnd.Next(100000000, 999999999);
                keys[1] = gadgetId;
                match = db.gadgets.Any(m => m.gadget_id == gadgetId);
            }
            while (match == true);
            return keys;
        }

        /// <summary>
        /// Creates a picture id for device Picture
        /// </summary>
        /// <returns>Picture Id</returns>
        public string PicIdGenerator()
        {
            Random rnd = new Random();
            string pictureId = "picture_";
            pictureId += rnd.Next(100000000, 999999999).ToString();
            return pictureId;
        }

        public string ActivityIdGenerator()
        {
            Random rnd = new Random();
            string activityId = "act";
            activityId += rnd.Next(100000000, 999999999).ToString();
            return activityId;
        }

        /// <summary>
        /// Saves picture to file server
        /// </summary>
        /// <param name="file"></param>
        /// <returns>File Name</returns>
        public string UpdatePicToFileServer(HttpPostedFile file)
        {
            var recFile = file;
            try
            {
                AmazonS3Client client = new AmazonS3Client(Amazon.RegionEndpoint.USWest2);
                string fileName = PicIdGenerator() + @".jpeg";
                var request = new PutObjectRequest
                {
                    BucketName = "prokart",
                    Key = fileName,
                    InputStream = recFile.InputStream
                };
                PutObjectResponse response = client.PutObject(request);                
                return fileName;
            }
            catch (Exception)
            {                
                return string.Empty;
            }
        }

        /// <summary>
        /// Insert new record to gadget table
        /// </summary>
        /// <param name="data"></param>
        public bool insertToGadgets(Device data, out string messageString, out int orderId)
        {
            try
            {
                string message = "";
                gadget gdet = new gadget();

                gdet.order_id = IdGenerator()[0];
                gdet.gadget_id = IdGenerator()[1];
                gdet.created_time = DateTime.Now;
                gdet.modified_time = gdet.created_time;
                gdet.brand = data.Brand;
                gdet.model = data.Model;
                gdet.ram = data.Ram;
                gdet.memory = data.Memory;
                gdet.camera = data.Camera;
                gdet.release_date = data.ReleaseDate;
                gdet.price = data.Price;
                gdet.availability = Availability.processing.ToString();
                gdet.battery_condition = data.BatteryCondition;
                gdet.screen_quality = data.ScreenQuality;
                gdet.charging_port_condition = data.ChargingPortCondition;
                gdet.camera_condition = data.CameraCondition;
                gdet.heating_problem = data.HeatingProblem;
                gdet.software_problem = data.SoftwareProblem;
                gdet.picture_name = data.PictureName;
                db.gadgets.Add(gdet);
                db.SaveChanges();                

                UpdateActivity(data.SessionId, data.FirstName, data.LastName, data.Email, data.Mobile, data.AddressOne, data.AddressTwo, data.Pin, gdet.order_id);
                ProcessEmail email = new ProcessEmail();
                bool success = email.ProcessSellEmail(data.Email, data.FirstName, gdet.order_id, out message);
                if (success)
                {
                    messageString = message;
                    orderId = gdet.order_id;
                    return true;
                }
                messageString = "failure";
                orderId = gdet.order_id;
                return true;
            }
            catch (Exception e)
            {                
                throw e;
            }
        }

        /// <summary>
        /// Retrives gadget table data
        /// </summary>
        /// <returns></returns>
        List<gadget> gadgetList = new List<ProkartWebService.gadget>();
        public List<gadget> getData()
        {
            var gdet = from gt in db.gadgets
                       orderby gt.modified_time descending
                       select gt;
            try
            {
                foreach (var v in gdet)
                {
                    gadget g = new ProkartWebService.gadget();
                    g.order_id = v.order_id;
                    g.created_time = v.created_time;
                    g.modified_time = v.modified_time;
                    g.brand = v.brand;
                    g.model = v.model;
                    g.release_date = v.release_date;
                    g.ram = v.ram;
                    g.memory = v.memory;
                    g.camera = v.camera;
                    g.price = v.price;
                    g.availability = v.availability;
                    g.battery_condition = v.battery_condition;
                    g.screen_quality = v.screen_quality;
                    g.charging_port_condition = v.charging_port_condition;
                    g.camera_condition = v.camera_condition;
                    g.heating_problem = v.heating_problem;
                    g.software_problem = v.software_problem;
                    gadgetList.Add(g);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Fetching table data failed due to : " + e.Message);
            }
            return gadgetList;
        }
        public UpdateRecords()
        {
        }

        /// <summary>
        /// Update record status in database
        /// </summary>
        /// <param name="selectedOrderIds"></param>
        /// <param name="status"></param>
        public UpdateRecords(long[] selectedOrderIds, string status)
        {
            try
            {
                for (int i = 0; i < selectedOrderIds.Length; i++)
                {
                    var temp = selectedOrderIds[i];
                    gadget gdet = db.gadgets.Single(match => match.order_id == temp);
                    gdet.availability = status;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        /// <summary>
        /// Update device specification in database
        /// </summary>
        /// <param name="selectedOrderIds"></param>
        /// <param name="status"></param>
        public UpdateRecords(List<Device> device)
        {
            try
            {
                for (int i = 0; i < device.Count; i++)
                {
                    var temp = device[i].OrderId;
                    gadget gdet = db.gadgets.Single(match => match.order_id == temp);
                    gdet.brand = device[i].Brand;
                    gdet.model = device[i].Model;
                    gdet.ram = device[i].Ram;
                    gdet.memory = device[i].Memory;
                    gdet.camera = device[i].Camera;
                    gdet.battery_condition = device[i].BatteryCondition;
                    gdet.screen_quality = device[i].ScreenQuality;
                    gdet.charging_port_condition = device[i].ChargingPortCondition;
                    gdet.camera_condition = device[i].CameraCondition;
                    gdet.heating_problem = device[i].HeatingProblem;
                    gdet.software_problem = device[i].SoftwareProblem;
                    gdet.price = device[i].Price;
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Getting available brands and records and
        /// all its overloads from database
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public List<gadget> GetAvailableRecords(int start, int end)
        {
            try
            {
                gadgetList.Clear();
                var gdet = from gt in db.gadgets
                           where gt.availability == "available"
                           select gt;
                foreach (var v in gdet)
                {
                    gadget g = new ProkartWebService.gadget();
                    g.order_id = v.order_id;
                    g.brand = v.brand;
                    g.model = v.model;
                    g.release_date = v.release_date;
                    g.ram = v.ram;
                    g.memory = v.memory;
                    g.camera = v.camera;
                    g.price = v.price;
                    g.picture_name = v.picture_name;
                    g.battery_condition = v.battery_condition;
                    g.screen_quality = v.screen_quality;
                    g.charging_port_condition = v.charging_port_condition;
                    g.camera_condition = v.camera_condition;
                    g.heating_problem = v.heating_problem;
                    g.software_problem = v.software_problem;
                    gadgetList.Add(g);
                }
                return gadgetList.Skip(start).Take(end - start + 1).ToList();
            }
            catch (Exception e)
            {
                gadgetList.Clear();
                throw e;
            }
        }
        public List<gadget> GetAvailableRecords(string[] brands)
        {
            try
            {
                gadgetList.Clear();
                var gdet = from gt in db.gadgets
                           where gt.availability == "available" && brands.Contains(gt.brand.ToUpper())
                           select gt;
                foreach (var v in gdet)
                {
                    gadget g = new ProkartWebService.gadget();
                    g.order_id = v.order_id;
                    g.brand = v.brand;
                    g.model = v.model;
                    g.release_date = v.release_date;
                    g.ram = v.ram;
                    g.memory = v.memory;
                    g.camera = v.camera;
                    g.price = v.price;
                    g.picture_name = v.picture_name;
                    g.battery_condition = v.battery_condition;
                    g.screen_quality = v.screen_quality;
                    g.charging_port_condition = v.charging_port_condition;
                    g.camera_condition = v.camera_condition;
                    g.heating_problem = v.heating_problem;
                    g.software_problem = v.software_problem;
                    gadgetList.Add(g);
                }
                return gadgetList;
            }
            catch (Exception e)
            {
                gadgetList.Clear();
                throw e;
            }
        }
        public List<gadget> GetAvailableBrands()
        {
            gadgetList.Clear();
            var gdet = from gt in db.gadgets
                       where gt.availability == "available"
                       select gt;
            try
            {
                foreach (var v in gdet)
                {
                    gadget g = new ProkartWebService.gadget();
                    g.order_id = v.order_id;
                    g.brand = v.brand;
                    g.price = v.price;
                    gadgetList.Add(g);
                }
                return gadgetList;
            }
            catch (Exception e)
            {
                Console.WriteLine("Fetching data failed due to : " + e.Message);
                gadgetList.Clear();
                return gadgetList;
            }
        }
        public void CreateActivity(string sessionId, string pictureName)
        {
            try
            {
                activity act = new activity();
                act.activity_id = ActivityIdGenerator();
                act.session_id = sessionId;
                act.picture_name = pictureName;
                act.activity_status = "N";
                db.activities.Add(act);
                db.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        System.Diagnostics.Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void UpdateActivity(string sessionId, string firstName, string lastName, string email, long mobile, string addressOne, string addressTwo, int pin, int orderId)
        {           
            try
            {
                if (sessionId != string.Empty)
                {                    
                    var activity = db.activities.SingleOrDefault(a => a.session_id == sessionId);
                    activity.first_name = firstName;
                    activity.last_name = lastName;
                    activity.email_address = email;
                    activity.mobile_number = mobile;
                    activity.address_line_one = addressOne;
                    activity.address_line_two = addressTwo;
                    activity.pin_code = pin;
                    activity.order_id = orderId;
                    activity.activity_status = "Y";
                    db.SaveChanges();                    
                }
                else
                {
                    activity activity = new ProkartWebService.activity();
                    activity.activity_id = ActivityIdGenerator();
                    activity.session_id = System.Web.HttpContext.Current.Session.SessionID;
                    activity.first_name = firstName;
                    activity.last_name = lastName;
                    activity.email_address = email;
                    activity.mobile_number = mobile;
                    activity.address_line_one = addressOne;
                    activity.address_line_two = addressTwo;
                    activity.pin_code = pin;
                    activity.order_id = orderId;
                    activity.activity_status = "Y";
                    db.activities.Add(activity);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using System.Configuration;

namespace ProKart.Models
{
    public class PriceCalculation
    {
        public string folderPath = "";
        public string fileName = "";
        /// Filtering internal memory, ram and camera values from input strings
        /// <auther>Pranab Kumar De</auther>
        /// <date created>06/05/2016</date created>
        /// </summary>
        /// <param name="memoryInput"></param>
        /// <param name="cameraInput"></param>
        /// <returns>device specification strings</returns>
        public List<string> getMemoryValues(string releaseDateString, string memoryInput, string cameraInput)
        {
            //Initializing local variables
            string releaseDateOutputValue = "";
            string memoryOutputValue = "";
            string cameraOutputValue = "";
            string ramOutputValue = "";
            List<string> lstData = new List<string>();
            try
            {
                //Extracting release date
                if (releaseDateString.Any(char.IsDigit))
                {
                    releaseDateOutputValue = Regex.Match(releaseDateString, @"\d+").Value;
                }
                else
                {
                    releaseDateOutputValue = "1990";
                }

                //Extracting memory values from input string to array of strings    
                string[] cameraValueStrings = cameraInput.Split(',').Select(camValue => camValue.Trim()).ToArray();
                string[] memoryValueStrings = memoryInput.Split(',').Select(mValue => mValue.Trim()).ToArray();

                string memoryValueStringsFirst = memoryValueStrings.First().ToString();
                string memoryValueStringsLast = memoryValueStrings.Last().ToString();
                string[] firstMemoryValueStrings = memoryValueStringsFirst.Split(' ').Select(iValue => iValue.Trim()).ToArray();
                string[] lastMemoryValueStrings = memoryValueStringsLast.Split(' ').Select(rValue => rValue.Trim()).ToArray();


                //Extracting internal memory value
                if (memoryValueStringsFirst.Any(char.IsDigit))
                {
                    if (memoryValueStringsFirst.Contains("RAM") == true)
                    {
                        memoryOutputValue = "0 MB";
                    }
                    else
                    {
                        for (int i = 0; i < (firstMemoryValueStrings.Length - 1); i++)
                        {
                            if (firstMemoryValueStrings[i].Any(char.IsDigit) && (firstMemoryValueStrings[i + 1] == "GB" || firstMemoryValueStrings[i + 1] == "MB"))
                            {
                                if (char.IsDigit(firstMemoryValueStrings[i], 0))
                                {
                                    string memoryDigits = new string(firstMemoryValueStrings[i].TakeWhile(char.IsDigit).ToArray());
                                    memoryOutputValue = string.Join(" ", memoryDigits, firstMemoryValueStrings[i + 1]);
                                    break;
                                }
                            }
                            else
                            {
                                memoryOutputValue = "0 MB";
                            }
                        }
                    }
                }
                else
                {
                    memoryOutputValue = "0 MB";
                }
                //Extracting RAM value
                if (memoryValueStringsLast.Any(char.IsDigit) && memoryValueStringsLast.Contains("RAM"))
                {
                    for (int j = 0; j < (lastMemoryValueStrings.Count() - 1); j++)
                    {
                        if (lastMemoryValueStrings[j].Any(char.IsDigit) && (lastMemoryValueStrings[j + 1] == "GB" || lastMemoryValueStrings[j + 1] == "MB"))
                        {
                            ramOutputValue = string.Join(" ", lastMemoryValueStrings[j], lastMemoryValueStrings[j + 1]);
                            break;
                        }
                        else
                        {
                            ramOutputValue = "0 MB";
                        }
                    }
                }
                else
                {
                    ramOutputValue = "0 MB";
                }
                //Extracting camera value

                for (int k = 0; k < cameraValueStrings.Count(); k++)
                {
                    if (cameraValueStrings[k].Any(char.IsDigit))
                    {
                        cameraOutputValue = cameraValueStrings[k];
                        break;
                    }
                    else
                    {
                        cameraOutputValue = "0 MP";
                        continue;
                    }
                }

                lstData.Add(releaseDateOutputValue);
                lstData.Add(ramOutputValue);
                lstData.Add(memoryOutputValue);
                lstData.Add(cameraOutputValue);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstData;
        }


        /// <summary>
        /// Price calculation method
        ///<Auther>Pranab Kumar De</Auther>
        ///<date created>06/05/2016</date created>
        /// </summary>
        /// <returns>device price</returns>
        public int priceCalculate(Device data)
        {
            int price = 0;
            try
            {
                //Initializing local variables
                int initialPrice = 0;
                int minPrice = 400;
                int currentYear = Convert.ToInt32(DateTime.Now.Year);
                int age = currentYear - data.ReleaseDate;
                List<string> tier1 = new List<string>();
                List<string> tier2 = new List<string>();
                List<string> tier3 = new List<string>();
                List<string> value = new List<string>();
                Brands brands = new Brands();
                SortedDictionary<string, List<string>> sd = brands.getBrandsFromDb();//Getting brands from Database
                //Price calculation based on Brand
                if (sd.TryGetValue("Tier 1", out value))
                {
                    tier1 = value;
                    if (tier1.Contains(data.Brand))
                    {
                        price = initialPrice + 5000;
                    }
                }
                if (sd.TryGetValue("Tier 2", out value))
                {
                    tier2 = value;
                    if (tier2.Contains(data.Brand))
                    {
                        price = initialPrice + 3000;
                    }
                }
                if (sd.TryGetValue("Tier 3", out value))
                {
                    tier3 = value;
                    if (tier3.Contains(data.Brand))
                    {
                        price = initialPrice + 100;
                    }
                }
                //Price calculation based on RAM memory
                if ((data.Ram.Substring(data.Ram.Length - 2)) == "GB")
                {
                    double ram = double.Parse(Regex.Match(data.Ram, @"\d+").Value);
                    price += Convert.ToInt32(500 * ram);
                }
                if ((data.Ram.Substring(data.Ram.Length - 2)) == "MB")
                {
                    double ram = double.Parse(Regex.Match(data.Ram, @"\d+").Value);
                    price += Convert.ToInt32(0.5 * ram);
                    if (tier1.Contains(data.Brand))
                    {
                        price -= 2000;
                    }

                    if (tier2.Contains(data.Brand))
                    {
                        price -= 3000;
                    }

                    if (tier3.Contains(data.Brand))
                    {
                        price -= 100;
                    }

                }
                //Price calculation based on Internal memory
                if ((data.Memory.Substring(data.Memory.Length - 2)) == "GB")
                {
                    string memoryValues = new string(data.Memory.TakeWhile(Char.IsDigit).ToArray());
                    int memory = int.Parse(memoryValues);
                    price += Convert.ToInt32(50 * memory);
                }
                if ((data.Memory.Substring(data.Memory.Length - 2)) == "MB")
                {
                    int memory = int.Parse(Regex.Match(data.Memory, @"\d+").Value);
                    price += Convert.ToInt32(0.1 * memory);
                    if (tier1.Contains(data.Brand) && price > 2000)
                    {
                        price -= 2000;
                    }
                    if (tier1.Contains(data.Brand) && price <= 2000)
                    {
                        price -= 1000;
                    }
                    if (tier2.Contains(data.Brand) && price > 3000)
                    {
                        price -= 3000;
                    }
                    if (tier2.Contains(data.Brand) && price <= 3000)
                    {
                        price -= 1500;
                    }
                    if (tier3.Contains(data.Brand) && price > 100)
                    {
                        price -= 100;
                    }
                    if (tier3.Contains(data.Brand) && price <= 100)
                    {
                        price -= 50;
                    }
                }
                //Price calculation based on Camera value
                string[] cameraValues = data.Camera.Split(' ').Select(cValue => cValue.Trim()).ToArray();
                if ((data.Camera.Substring(data.Camera.Length - 2)) == "MP" && cameraValues[0] == "Dual")
                {
                    double cameraValue = double.Parse(Regex.Match(data.Camera, @"\d+").Value);
                    cameraValue *= 3;
                    price += Convert.ToInt32(100 * cameraValue);
                }
                if ((data.Camera.Substring(data.Camera.Length - 2)) == "MP" && cameraValues[0] != "Dual")
                {
                    double cameraValue = double.Parse(Regex.Match(data.Camera, @"\d+").Value);
                    price += Convert.ToInt32(100 * cameraValue);
                }
                //Price calculation based on the age of the device (from release date to current date)
                if (age < 5 && tier1.Contains(data.Brand))
                {
                    price += 5000;
                    int percentage = age * age * age;
                    int percentageValue = 5000 * percentage;
                    int deductable = percentageValue / 100;
                    price -= deductable;
                }
                if (age < 5 && tier2.Contains(data.Brand))
                {
                    price += 3000;
                    int percentage = age * age * age;
                    int percentageValue = 3000 * percentage;
                    int deductable = percentageValue / 100;
                    price -= deductable;
                }
                if (age < 5 && tier3.Contains(data.Brand))
                {
                    int percentage = age * age * age;
                    int percentageValue = 8000 * percentage;
                    int deductable = percentageValue / 100;
                    price -= deductable;
                    if (price < 1000 && (data.Ram.Substring(data.Ram.Length - 2)) == "GB" && (data.Memory.Substring(data.Memory.Length - 2)) == "GB")
                    {
                        price += deductable;
                    }
                }
                if (age >= 5 && tier3.Contains(data.Brand))
                {
                    price -= 3000;
                }

                //Price calculation based on device condition given by user
                if (price > minPrice)
                {
                    price -= 550 - (50 * data.BatteryCondition);
                }
                if (price > minPrice)
                {
                    price -= 550 - (50 * data.ScreenQuality);
                }
                if (price > minPrice)
                {
                    price -= 550 - (50 * data.ChargingPortCondition);
                }
                if (price > minPrice)
                {
                    price -= 550 - (50 * data.CameraCondition);
                }
                if (price > minPrice)
                {
                    price -= 550 - (50 * data.HeatingProblem);
                }
                if (price > minPrice)
                {
                    price -= 550 - (50 * data.SoftwareProblem);
                }
                if (price < minPrice)
                {
                    price = minPrice;
                }

                //Normalizing device selling value based on current market price
                //
                //Tier 3
                if (tier3.Contains(data.Brand) && (age == 0) && price > 2000 && data.ActualPrice > 0)
                {
                    data.ActualPrice = Convert.ToInt32(data.ActualPrice / 2.5);
                    price = Convert.ToInt32((price + data.ActualPrice) / 2);
                }
                if (tier3.Contains(data.Brand) && (age == 1) && price > 2000 && data.ActualPrice > 0)
                {
                    data.ActualPrice = Convert.ToInt32(data.ActualPrice / 4.5);
                    price = Convert.ToInt32((price + data.ActualPrice) / 2);
                }
                if (tier3.Contains(data.Brand) && (age >= 2) && price > 2000 && data.ActualPrice > 0)
                {
                    data.ActualPrice = Convert.ToInt32(data.ActualPrice / 6.5);
                    price = Convert.ToInt32((price + data.ActualPrice) / 2);
                }
                //Tier 2
                if (tier2.Contains(data.Brand) && (age == 0) && price > 5000 && data.ActualPrice > 0)
                {
                    data.ActualPrice = Convert.ToInt32(data.ActualPrice / 1.67);
                    price = Convert.ToInt32((price + data.ActualPrice) / 2);
                }
                if (tier2.Contains(data.Brand) && (age == 1) && price > 5000 && data.ActualPrice > 0)
                {
                    data.ActualPrice = Convert.ToInt32(data.ActualPrice / 2);
                    price = Convert.ToInt32((price + data.ActualPrice) / 2);
                }
                if (tier2.Contains(data.Brand) && (age == 2) && price > 5000 && data.ActualPrice > 0)
                {
                    data.ActualPrice = Convert.ToInt32(data.ActualPrice / 3.33);
                    price = Convert.ToInt32((price + data.ActualPrice) / 2);
                }
                if (tier2.Contains(data.Brand) && (age >= 3) && price > 4000 && data.ActualPrice > 0)
                {
                    data.ActualPrice = Convert.ToInt32(data.ActualPrice / 5);
                    price = Convert.ToInt32((price + data.ActualPrice) / 2);
                }

                //Tier 1
                if (tier1.Contains(data.Brand) && (age < 5) && price > 2000 && data.ActualPrice > 0)
                {
                    data.ActualPrice = Convert.ToInt32(data.ActualPrice / 2.2);
                    price = Convert.ToInt32((price + data.ActualPrice) / 2);
                }
                if (tier1.Contains(data.Brand) && (age > 5) && price > 2000 && data.ActualPrice > 0)
                {
                    data.ActualPrice = Convert.ToInt32(data.ActualPrice / 3.7);
                    price = Convert.ToInt32((price + data.ActualPrice) / 2);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return price;
        }
    }
}
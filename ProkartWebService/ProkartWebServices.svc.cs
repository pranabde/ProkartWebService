﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Newtonsoft.Json;
using ProKart.Models;
using System.IO;
using System.Web;
using System.Net;
[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace ProkartWebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "ProkartWebServices" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select ProkartWebServices.svc or ProkartWebServices.svc.cs at the Solution Explorer and start debugging.
    public class ProkartWebServices : IProkartWebServices
    {        
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        //Local variables
        public string message = "";
        public string messageToController = "";
        public string pictureName = "";
        public int orderId = 0;

        ///<summary>Sell Services</summary>
        ///<remarks>Services related to Sell page</remarks>
        ///
        public string SetDeviceDetails(Device data)
        {
            try
            {
                var reply = new
                {
                    Brand = data.Brand,
                    Model = data.Model,
                    ModelId = data.ModelId,
                    PictureLink = data.PictureLink
                };
                return JsonConvert.SerializeObject(reply);
            }
            catch (Exception exc)
            {
                ProkartCustomException ex = new ProkartCustomException();
                ProkartCustomException prokartException = new ProkartCustomException();
                prokartException.ExceptionMessage = "Message: " + exc.Message + Environment.NewLine + "Stack Trace: " + exc.StackTrace;
                prokartException.Time = DateTime.Now.ToString();
                prokartException.ServiceName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
                FaultException<ProkartCustomException> fe = new FaultException<ProkartCustomException>(prokartException, new FaultReason(exc.Message));
                throw fe;
            }
        }

        public string SetDeviceDetailsAfter(Device data)
        {
            try
            {
                var reply = new
                {
                    Brand = data.Brand,
                    Model = data.Model,
                    Ram = data.Ram,
                    Memory = data.Memory,
                    Camera = data.Camera,
                    ReleaseDate = data.ReleaseDate,
                    BatteryCondition = data.BatteryCondition,
                    ScreenQuality = data.ScreenQuality,
                    ChargingPortCondition = data.ChargingPortCondition,
                    CameraCondition = data.CameraCondition,
                    HeatingProblem = data.HeatingProblem,
                    SoftwareProblem = data.SoftwareProblem,
                    Price = data.Price,
                    PictureName = data.PictureName
                };
                return JsonConvert.SerializeObject(reply);
            }
            catch (Exception exc)
            {
                ProkartCustomException prokartException = new ProkartCustomException();
                prokartException.ExceptionMessage = "Message: " + exc.Message + Environment.NewLine + "Stack Trace: " + exc.StackTrace;
                prokartException.Time = DateTime.Now.ToString();
                prokartException.ServiceName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
                FaultException<ProkartCustomException> fe = new FaultException<ProkartCustomException>(prokartException, new FaultReason(exc.Message));
                throw fe;
            }
        }

        public string SetDeviceSpecification(Device data)
        {
            try
            {
                List<string> deviceData;
                PriceCalculation priceCalculation = new PriceCalculation();
                deviceData = priceCalculation.getMemoryValues(data.ReleaseDateInput, data.MemoryInput, data.CameraInput);
                var reply = new
                {
                    Brand = data.Brand,
                    Model = data.Model,
                    ReleaseDate = Convert.ToInt32(deviceData.ElementAt(0)),
                    Ram = deviceData.ElementAt(1),
                    Memory = deviceData.ElementAt(2),
                    Camera = deviceData.ElementAt(3)
                };
                return JsonConvert.SerializeObject(reply);
            }
            catch (Exception exc)
            {
                ProkartCustomException prokartException = new ProkartCustomException();
                prokartException.ExceptionMessage = "Message: " + exc.Message + Environment.NewLine + "Stack Trace: " + exc.StackTrace;
                prokartException.Time = DateTime.Now.ToString();
                prokartException.ServiceName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
                FaultException<ProkartCustomException> fe = new FaultException<ProkartCustomException>(prokartException, new FaultReason(exc.Message));
                throw fe;
            }
        }

        public string GetDevicePrice(Device data)
        {
            try
            {
                PriceCalculation priceCalculation = new PriceCalculation();
                var reply = new
                {
                    Brand = data.Brand,
                    Model = data.Model,
                    ReleaseDate = data.ReleaseDate,
                    Ram = data.Ram,
                    Memory = data.Memory,
                    Camera = data.Camera,
                    Price = priceCalculation.priceCalculate(data),
                    BatteryCondition = data.BatteryCondition,
                    ScreenQuality = data.ScreenQuality,
                    ChargingPortCondition = data.ChargingPortCondition,
                    CameraCondition = data.CameraCondition,
                    HeatingProblem = data.HeatingProblem,
                    SoftwareProblem = data.SoftwareProblem,
                    PictureName = data.PictureName
                };
                return JsonConvert.SerializeObject(reply);
            }
            catch (Exception exc)
            {
                ProkartCustomException prokartException = new ProkartCustomException();
                prokartException.ExceptionMessage = "Message: " + exc.Message + Environment.NewLine + "Stack Trace: " + exc.StackTrace;
                prokartException.Time = DateTime.Now.ToString();
                prokartException.ServiceName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
                FaultException<ProkartCustomException> fe = new FaultException<ProkartCustomException>(prokartException, new FaultReason(exc.Message));
                throw fe;
            }
        }

        public string CheckOut(Device data)
        {
            try
            {
                UpdateRecords records = new UpdateRecords();
                bool success = records.insertToGadgets(data, out message, out orderId);                
                if (success)
                {
                    if (message == "success")
                    {
                        messageToController = @"Your request is processed. Order Id is " + orderId.ToString();
                    }
                    if (message == "failure")
                    {
                        messageToController = @"Your request is processed. Order Id is " + orderId.ToString() + @" but we couldn't send email to " + data.Email.ToString();
                    }
                }                
                var reply = new
                {
                    FirstName = data.FirstName,
                    Message = messageToController
                };
                return JsonConvert.SerializeObject(reply);
            }
            catch (Exception exc)
            {
                ProkartCustomException prokartException = new ProkartCustomException();
                prokartException.ExceptionMessage = "Message: " + exc.Message + Environment.NewLine + "Stack Trace: " + exc.StackTrace;
                prokartException.Time = DateTime.Now.ToString();
                prokartException.ServiceName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
                FaultException<ProkartCustomException> fe = new FaultException<ProkartCustomException>(prokartException, new FaultReason(exc.Message));
                throw fe;
            }
        }

        //Browser service
        public string GetDevicePic()
        {
            string sessionid = System.Web.HttpContext.Current.Session.SessionID;
            UpdateRecords records = new UpdateRecords();
            try
            {
                if (System.Web.HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    string[] extensions = { ".jpg", ".jpeg", ".gif", ".bmp", ".png" };
                    var firstPic = System.Web.HttpContext.Current.Request.Files["FirstImage"];
                    if (!extensions.Any(x => x.Equals(Path.GetExtension(firstPic.FileName.ToLower()), StringComparison.OrdinalIgnoreCase)))
                    {
                        throw new Exception("Incorrect File Format.");
                    }
                    string pictureOneName = records.UpdatePicToFileServer(firstPic);
                    if (pictureOneName == string.Empty)
                    {
                        throw new Exception("File Upload Failure.");
                    }
                    pictureName = pictureOneName;                    
                    if (System.Web.HttpContext.Current.Request.Files["SecondImage"] != null)
                    {
                        var secondPic = System.Web.HttpContext.Current.Request.Files["SecondImage"];
                        if (!extensions.Any(x => x.Equals(Path.GetExtension(secondPic.FileName.ToLower()), StringComparison.OrdinalIgnoreCase)))
                        {
                            throw new Exception("Incorrect File Format.");
                        }
                        string pictureTwoName = records.UpdatePicToFileServer(secondPic);
                        if (pictureTwoName == string.Empty)
                        {
                            throw new Exception("File Upload Failure.");
                        }
                        pictureName += @"," + pictureTwoName;
                    }
                }
                records.CreateActivity(sessionid, pictureName);
                var reply = new
                {
                    PictureName = pictureName,
                    SessionId = sessionid
                };
                return JsonConvert.SerializeObject(reply);
            }
            catch (Exception e)
            {
                log.Error("File Upload Failure in GetDevicePic()." + e);
                return "File Upload Failure.";
            }
        }

        public string SetDeviceDetailsForOtherDevices(Device data)
        {
            try
            {
                var reply = new
                {
                    Brand = data.Brand,
                    Model = data.Model,
                    Ram = data.Ram,
                    Memory = data.Memory,
                    Camera = data.Camera,
                    ReleaseDate = data.ReleaseDate
                };
                return JsonConvert.SerializeObject(reply);
            }
            catch (Exception exc)
            {
                ProkartCustomException prokartException = new ProkartCustomException();
                prokartException.ExceptionMessage = "Message: " + exc.Message + Environment.NewLine + "Stack Trace: " + exc.StackTrace;
                prokartException.Time = DateTime.Now.ToString();
                prokartException.ServiceName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
                FaultException<ProkartCustomException> fe = new FaultException<ProkartCustomException>(prokartException, new FaultReason(exc.Message));
                throw fe;
            }
        }


        ///<summary>Buy Services</summary>
        ///<remarks>Services related to Buy page</remarks>
        ///--------------------------------------------------------------------------------------
        public string GetAllAvailableItems(int[] index)
        {
            try
            {
                UpdateRecords records = new UpdateRecords();
                int startIndex = index[0];
                int endIndex = index[1];
                return JsonConvert.SerializeObject(records.GetAvailableRecords(startIndex, endIndex));
            }
            catch (Exception exc)
            {
                ProkartCustomException prokartException = new ProkartCustomException();
                prokartException.ExceptionMessage = "Message: " + exc.Message + Environment.NewLine + "Stack Trace: " + exc.StackTrace;
                prokartException.Time = DateTime.Now.ToString();
                prokartException.ServiceName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
                FaultException<ProkartCustomException> fe = new FaultException<ProkartCustomException>(prokartException, new FaultReason(exc.Message));
                throw fe;
            }
        }

        public string GetSortedItems(string[] brands)
        {
            try
            {
                UpdateRecords records = new UpdateRecords();
                return JsonConvert.SerializeObject(records.GetAvailableRecords(brands));
            }
            catch (Exception exc)
            {
                ProkartCustomException prokartException = new ProkartCustomException();
                prokartException.ExceptionMessage = "Message: " + exc.Message + Environment.NewLine + "Stack Trace: " + exc.StackTrace;
                prokartException.Time = DateTime.Now.ToString();
                prokartException.ServiceName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
                FaultException<ProkartCustomException> fe = new FaultException<ProkartCustomException>(prokartException, new FaultReason(exc.Message));
                throw fe;
            }
        }

        //Browser service
        public string GetAllAvailableBrands()
        {
            try
            {
                UpdateRecords records = new UpdateRecords();
                return JsonConvert.SerializeObject(records.GetAvailableBrands());
            }
            catch (Exception e)
            {
                log.Error("Error in GetAllAvailableBrands()." + e);
                return string.Empty;
            }
        }


        ///<summary>Admin Services</summary>
        ///<remarks>Services related to Admin page</remarks>
        ///---------------------------------------------------------------------------------------
        public void SetBrands(string[] Tier1, string[] Tier2, string[] Tier3)
        {
            string[] tier1;
            string[] tier2;
            string[] tier3;
            try
            {
                if (Tier1[0] == "")
                {
                    tier1 = Tier1.Skip(1).ToArray();
                }
                else
                {
                    tier1 = Tier1;
                }
                if (Tier2[0] == "")
                {
                    tier2 = Tier2.Skip(1).ToArray();
                }
                else
                {
                    tier2 = Tier2;
                }
                if (Tier3[0] == "")
                {
                    tier3 = Tier3.Skip(1).ToArray();
                }
                else
                {
                    tier3 = Tier3;
                }
                Brands brands = new Brands();
                brands.updateBrandsToDb(tier1, tier2, tier3);
            }
            catch (Exception exc)
            {
                ProkartCustomException prokartException = new ProkartCustomException();
                prokartException.ExceptionMessage = "Message: " + exc.Message + Environment.NewLine + "Stack Trace: " + exc.StackTrace;
                prokartException.Time = DateTime.Now.ToString();
                prokartException.ServiceName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
                FaultException<ProkartCustomException> fe = new FaultException<ProkartCustomException>(prokartException, new FaultReason(exc.Message));
                throw fe;
            }
        }

        //Browser service
        public string GetBrands()
        {
            List<string> tier1 = new List<string>();
            List<string> tier2 = new List<string>();
            List<string> tier3 = new List<string>();
            List<string> value = new List<string>();
            Brands brands = new Brands();
            try
            {
                SortedDictionary<string, List<string>> sd = brands.getBrandsFromDb();

                if (sd.TryGetValue("Tier 1", out value))
                {
                    tier1 = value;
                }
                if (sd.TryGetValue("Tier 2", out value))
                {
                    tier2 = value;
                }
                if (sd.TryGetValue("Tier 3", out value))
                {
                    tier3 = value;
                }

                var reply = new
                {
                    Tier1 = tier1,
                    Tier2 = tier2,
                    Tier3 = tier3
                };
                return JsonConvert.SerializeObject(reply);
            }
            catch (Exception e)
            {
                log.Error("Error in GetBrands()." + e);
                return string.Empty;
            }
        }

        //Browser service
        public string GetGadgetData()
        {
            try
            {
                UpdateRecords records = new UpdateRecords();
                return JsonConvert.SerializeObject(records.getData());
            }
            catch (Exception e)
            {
                log.Error("Error in GetBrands()." + e);
                return string.Empty;
            }
        }

        public void UpdateGadgetStatus(long[] orderIds, string status)
        {
            try
            {
                UpdateRecords records = new UpdateRecords(orderIds, status);
            }
            catch (Exception exc)
            {
                ProkartCustomException prokartException = new ProkartCustomException();
                prokartException.ExceptionMessage = "Message: " + exc.Message + Environment.NewLine + "Stack Trace: " + exc.StackTrace;
                prokartException.Time = DateTime.Now.ToString();
                prokartException.ServiceName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
                FaultException<ProkartCustomException> fe = new FaultException<ProkartCustomException>(prokartException, new FaultReason(exc.Message));
                throw fe;
            }
        }

        public void UpdateGadgetDetails(List<Device> data)
        {
            try
            {
                UpdateRecords records = new UpdateRecords(data);
            }
            catch (Exception exc)
            {
                ProkartCustomException prokartException = new ProkartCustomException();
                prokartException.ExceptionMessage = "Message: " + exc.Message + Environment.NewLine + "Stack Trace: " + exc.StackTrace;
                prokartException.Time = DateTime.Now.ToString();
                prokartException.ServiceName = System.Reflection.MethodBase.GetCurrentMethod().ToString();
                FaultException<ProkartCustomException> fe = new FaultException<ProkartCustomException>(prokartException, new FaultReason(exc.Message));
                throw fe;
            }
        }

    }
    /// <summary>
    /// Services ends here
    /// -----------------------------------------------------------------------------------------
    /// </summary>
    public class WcfReadEntityBodyModeProkartModule : IHttpModule
    {
        public void Dispose()
        {
        }
        public void Init(HttpApplication context)
        {
            context.BeginRequest += context_BeginRequest;
        }

        void context_BeginRequest(object sender, EventArgs e)
        {
            //This will force the HttpContext.Request.ReadEntityBody to be “Classic” and will ensure compatibility..
            Stream stream = (sender as HttpApplication).Request.InputStream;
        }
    }
}

﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ProKart.Models
{
    /// <summary>
    /// <auther>Pranab Kumar De</auther>
    /// <created>26/04/2016</created>
    /// <remarks>public property declaration</remarks>
    /// </summary>
    [DataContract]
    public class Device
    {
        [DataMember]
        public string Brand { get; set; }
        [DataMember]
        public string Model { get; set; }
        [DataMember]
        public Int16 ModelId { get; set; }
        [DataMember]
        public string PictureLink { get; set; }
        [DataMember]
        public string ReleaseDateInput { get; set; }
        [DataMember]
        public string MemoryInput { get; set; }
        [DataMember]
        public string CameraInput { get; set; }
        [DataMember]
        public string Ram { get; set; }
        [DataMember]
        public string Memory { get; set; }
        [DataMember]
        public string Camera { get; set; }
        [DataMember]
        public int ReleaseDate { get; set; }
        [DataMember]
        public sbyte BatteryCondition { get; set; }
        [DataMember]
        public sbyte ScreenQuality { get; set; }
        [DataMember]
        public sbyte ChargingPortCondition { get; set; }
        [DataMember]
        public sbyte CameraCondition { get; set; }
        [DataMember]
        public sbyte HeatingProblem { get; set; }
        [DataMember]
        public sbyte SoftwareProblem { get; set; }
        [DataMember]
        public int ActualPrice { get; set; }
        [DataMember]
        public int Price { get; set; }
        [DataMember]
        public string FirstName { get; set; }
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public long Mobile { get; set; }
        [DataMember]
        public string AddressOne { get; set; }
        [DataMember]
        public string AddressTwo { get; set; }
        [DataMember]
        public int Pin { get; set; }
        [DataMember]
        public Image PicData { get; set; }
        [DataMember]
        public string PictureName { get; set; }
        [DataMember]
        public long OrderId { get; set; }
        [DataMember]
        public string SessionId { get; set; }
    }
}
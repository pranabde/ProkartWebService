﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ProkartWebService
{
    [DataContract]
    public class ProkartCustomException
    {
        [DataMember]
        public string Time { get; set; }
        [DataMember]
        public string ExceptionMessage { get; set; }
        [DataMember]
        public string ServiceName { get; set; }
    }
}